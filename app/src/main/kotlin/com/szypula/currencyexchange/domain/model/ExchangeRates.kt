package com.szypula.currencyexchange.domain.model

data class ExchangeRates(
    val base: String,
    val rates: List<Rate>
)

data class Rate(
    val name: String,
    val value: Double
): Comparable<Rate> {

    override fun compareTo(other: Rate): Int = name.compareTo(other.name)
}