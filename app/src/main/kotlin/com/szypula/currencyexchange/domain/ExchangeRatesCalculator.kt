package com.szypula.currencyexchange.domain

import com.szypula.currencyexchange.domain.model.ExchangeRates
import com.szypula.currencyexchange.domain.model.Rate

fun ExchangeRates.calculateExchangeRates(baseValue: Double): ExchangeRates =
    copy(rates = rates.map { it * baseValue })

private operator fun Rate.times(baseValue: Double): Rate = copy(value = value * baseValue)

fun ExchangeRates.changeBaseCurrency(baseCurrency: String): ExchangeRates =
    if (hasBaseCurrencyChanged(baseCurrency)) {
        ExchangeRates(
            baseCurrency,
            rates.copyAllBut(baseCurrency).sorted()
        )
    } else {
        this
    }

private fun ExchangeRates.hasBaseCurrencyChanged(baseCurrency: String) =
    rates.isNotEmpty() && base != baseCurrency

private fun List<Rate>.copyAllBut(baseCurrency: String) =
    filter { it.name != baseCurrency }
