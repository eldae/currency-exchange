package com.szypula.currencyexchange.domain

import com.szypula.currencyexchange.domain.model.ExchangeRatesState
import io.reactivex.Observable

interface ExchangeRatesInteractor {

    fun lastExchangeRatesValue(baseCurrency: String, baseValue: Double): ExchangeRatesState.Value?

    fun observeExchangeRate(baseCurrency: String, baseValue: Double): Observable<ExchangeRatesState>
}