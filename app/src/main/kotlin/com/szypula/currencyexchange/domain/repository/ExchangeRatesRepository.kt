package com.szypula.currencyexchange.domain.repository

import com.szypula.currencyexchange.domain.model.ExchangeRates
import io.reactivex.Single

interface ExchangeRatesRepository {

    fun fetchExchangeRates(base: String): Single<ExchangeRates>
}