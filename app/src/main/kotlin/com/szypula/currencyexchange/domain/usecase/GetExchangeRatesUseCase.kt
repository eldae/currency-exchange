package com.szypula.currencyexchange.domain.usecase

import com.szypula.currencyexchange.domain.model.ExchangeRatesState
import io.reactivex.Observable

interface GetExchangeRatesUseCase {

    fun observeState(baseCurrency: String): Observable<ExchangeRatesState>
}