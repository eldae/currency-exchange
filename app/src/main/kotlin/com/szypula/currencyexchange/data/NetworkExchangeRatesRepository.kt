package com.szypula.currencyexchange.data

import com.szypula.currencyexchange.domain.model.ExchangeRates
import com.szypula.currencyexchange.domain.model.Rate
import com.szypula.currencyexchange.domain.repository.ExchangeRatesRepository
import com.szypula.currencyexchange.data.model.RatesResponse
import io.reactivex.Single

class NetworkExchangeRatesRepository(
    private val ratesAPI: RatesAPI
) : ExchangeRatesRepository {

    override fun fetchExchangeRates(base: String): Single<ExchangeRates> =
        ratesAPI
            .fetchLatestRates(base)
            .map(::toExchangeRates)

    private fun toExchangeRates(response: RatesResponse) =
        ExchangeRates(
            response.base,
            response.rates.map(::toRate)
        )

    private fun toRate(entry: Map.Entry<String, Double>) =
        Rate(
            entry.key,
            entry.value
        )
}