package com.szypula.currencyexchange.data.model

data class RatesResponse(
    val base: String,
    val date: String,
    val rates: Map<String, Double>
)

