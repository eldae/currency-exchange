package com.szypula.currencyexchange.data

import com.szypula.currencyexchange.domain.model.ExchangeRates
import com.szypula.currencyexchange.domain.model.Rate
import com.szypula.currencyexchange.domain.repository.ExchangeRatesRepository
import io.reactivex.Single
import kotlin.random.Random

/**
 * A wrapper over 'real' [ExchangeRatesRepository] that simulates conditions of the original interview assignment.
 * The endpoint should return different values for every request.
 * It achieves that by adding a random value between -0.99 and 0.99 to every exchange rate
 * returned from the endpoint.
 */
class RandomizingExchangeRatesRepository(
    private val exchangeRatesRepository: ExchangeRatesRepository
) : ExchangeRatesRepository {

    override fun fetchExchangeRates(base: String): Single<ExchangeRates> =
        exchangeRatesRepository.fetchExchangeRates(base)
            .map(::toRandom)

    private fun toRandom(exchangeRates: ExchangeRates) =
        exchangeRates.copy(rates = exchangeRates.rates.map(::toRandomRate))

    private fun toRandomRate(rate: Rate) =
        rate.copy(value = rate.value + Random.nextDouble(-0.99, 0.99))
}