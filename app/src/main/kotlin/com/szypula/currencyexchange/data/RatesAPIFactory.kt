package com.szypula.currencyexchange.data

import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit.MILLISECONDS

private const val HEADER_API_KEY = "apikey"

object RatesAPIFactory {

    fun getRatesAPI(
        baseUrl: String,
        apiKey: String,
        timeoutInMilliseconds: Long
    ): RatesAPI {
        val okHttpClient = OkHttpClient
            .Builder()
            .readTimeout(timeoutInMilliseconds, MILLISECONDS)
            .addInterceptor(HttpLoggingInterceptor().apply { level = HttpLoggingInterceptor.Level.BODY })
            .addInterceptor { chain ->
                val request = chain.request().newBuilder()
                    .header(HEADER_API_KEY, apiKey)
                    .build()

                chain.proceed(request)
            }
            .build()

        return Retrofit.Builder()
            .baseUrl(baseUrl)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .client(okHttpClient)
            .build()
            .create(RatesAPI::class.java)
    }
}