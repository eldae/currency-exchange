package com.szypula.currencyexchange.data

import com.szypula.currencyexchange.data.model.RatesResponse
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface RatesAPI {

    @GET("fixer/latest")
    fun fetchLatestRates(
        @Query("base") base: String
    ): Single<RatesResponse>
}