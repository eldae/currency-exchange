package com.szypula.currencyexchange.presentation.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import com.szypula.currencyexchange.domain.ExchangeRatesInteractor
import com.szypula.currencyexchange.domain.model.ExchangeRatesState
import com.szypula.currencyexchange.domain.model.ExchangeRatesState.Error
import com.szypula.currencyexchange.domain.model.ExchangeRatesState.Loading
import com.szypula.currencyexchange.presentation.viewmodel.UIEvent.CurrenciesHidden
import com.szypula.currencyexchange.presentation.viewmodel.UIEvent.CurrenciesShown
import com.szypula.currencyexchange.presentation.viewmodel.UIEvent.CurrencySelected
import com.szypula.currencyexchange.presentation.viewmodel.UIEvent.ValueChanged
import com.szypula.currencyexchange.presentation.viewmodel.mapper.StateToViewStateMapper
import io.reactivex.Observable
import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import java.util.concurrent.TimeUnit.MILLISECONDS

class CurrenciesViewModel(
    private val refreshRateInMillis: Long,
    baseCurrency: String,
    baseValue: Double,
    private val exchangeRatesInteractor: ExchangeRatesInteractor,
    private val stateToViewStateMapper: StateToViewStateMapper,
    private val computationScheduler: Scheduler = Schedulers.computation(),
    private val mainThreadScheduler: Scheduler = AndroidSchedulers.mainThread()
) : ViewModel(),
    Observer<UIEvent> {

    val viewState: LiveData<ViewState>
        get() = _viewState

    private val _viewState = MutableLiveData<ViewState>()
    private val disposables = CompositeDisposable()
    private var currentViewState = ViewState.EMPTY

    private var value = baseValue
    private var uiValue = baseValue.toString()
    private var currency = baseCurrency

    override fun onChanged(uiEvent: UIEvent) {
        when (uiEvent) {
            is CurrenciesShown -> getExchangeRates(currency, value)
            is CurrencySelected -> {
                currency = uiEvent.currency
                cacheValue(uiEvent.value)

                unsubscribe()
                emitViewStateForNewCurrency()
                getExchangeRates(currency, value)
            }
            is ValueChanged -> {
                cacheValue(uiEvent.value)

                unsubscribe()
                emitViewStateForNewCurrency()
                getExchangeRates(currency, value)
            }
            is CurrenciesHidden -> unsubscribe()
        }
    }

    private fun cacheValue(newValue: String) {
        uiValue = newValue
        value = uiValue.toDoubleOrNull() ?: 0.0
    }

    private fun getExchangeRates(currency: String, value: Double) {
        exchangeRatesInteractor
            .observeExchangeRate(currency, value)
            .getValueEvery(refreshRateInMillis)
            .map { stateToViewStateMapper.map(it, uiValue) }
            .observeOn(mainThreadScheduler)
            .doOnNext(_viewState::setValue)
            .cacheCurrentViewState()
            .subscribe()
            .let(disposables::add)
    }

    private fun Observable<ExchangeRatesState>.getValueEvery(refreshRateInMillis: Long): Observable<ExchangeRatesState> =
        buffer(refreshRateInMillis, MILLISECONDS, computationScheduler)
            .map { it.lastOrNull() ?: Loading }
            .onErrorReturn(::Error)

    private fun Observable<ViewState>.cacheCurrentViewState() =
        doOnNext { currentViewState = it }

    private fun emitViewStateForNewCurrency() {
        exchangeRatesInteractor
            .lastExchangeRatesValue(currency, value)
            ?.let { stateToViewStateMapper.map(it, uiValue) }
            ?.let(_viewState::setValue)
    }

    override fun onCleared() = unsubscribe()

    private fun unsubscribe() = disposables.clear()
}