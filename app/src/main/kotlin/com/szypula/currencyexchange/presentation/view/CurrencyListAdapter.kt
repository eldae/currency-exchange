package com.szypula.currencyexchange.presentation.view

import android.text.Editable
import android.text.Selection
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.szypula.currencyexchange.R
import com.szypula.currencyexchange.presentation.viewmodel.Currency

class CurrencyListAdapter(
    private val onCurrencySelected: (String, String) -> Unit,
    private val onValueChanged: (String) -> Unit
) :
    RecyclerView.Adapter<CurrencyListAdapter.ViewHolder>() {

    var currencies: List<Currency> = emptyList()
        set(value) {
            DiffUtil
                .calculateDiff(CurrenciesDiffCallback(field, value))
                .dispatchUpdatesTo(this)
            field = value
        }

    private val textWatcher = object : TextWatcher {

        override fun afterTextChanged(s: Editable) {
            Selection.setSelection(s, s.length)
        }

        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            // do nothing
        }

        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            onValueChanged(s.toString())
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater
            .from(parent.context)
            .inflate(R.layout.currency_list_item, parent, false)

        return ViewHolder(view)
    }

    override fun getItemCount(): Int = currencies.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        require(currencies.size > position)

        with(currencies[position]) {
            holder.name.text = name
            holder.value.setText(value)
            holder.value.setOnFocusChangeListener { v, hasFocus ->
                val valueField = (v as EditText)
                if (hasFocus) {
                    onCurrencySelected(
                        name,
                        valueField.text.toString()
                    )
                    valueField.addTextChangedListener(textWatcher)
                } else {
                    valueField.removeTextChangedListener(textWatcher)
                }
            }
        }
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        val name: TextView = itemView.findViewById(R.id.name)
        val value: EditText = itemView.findViewById(R.id.value)
    }
}