package com.szypula.currencyexchange.presentation.viewmodel.mapper

import com.szypula.currencyexchange.domain.model.ExchangeRatesState
import com.szypula.currencyexchange.presentation.viewmodel.ViewState

interface StateToViewStateMapper {

    fun map(state: ExchangeRatesState, value: String): ViewState
}