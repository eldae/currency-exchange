package com.szypula.currencyexchange.presentation.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.szypula.currencyexchange.Config
import com.szypula.currencyexchange.domain.IntervalExchangeRatesInteractor
import com.szypula.currencyexchange.domain.usecase.IntervalGetExchangeRatesUseCase
import com.szypula.currencyexchange.data.NetworkExchangeRatesRepository
import com.szypula.currencyexchange.data.RandomizingExchangeRatesRepository
import com.szypula.currencyexchange.data.RatesAPIFactory
import com.szypula.currencyexchange.presentation.viewmodel.mapper.CachingStateToViewStateMapper

class CurrenciesViewModelFactory(
    private val config: Config
) : ViewModelProvider.Factory {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel> create(modelClass: Class<T>): T = with(config) {
        val ratesAPI = RatesAPIFactory.getRatesAPI(
            baseUrl = endpoint,
            apiKey = accessKey,
            timeoutInMilliseconds = requestTimeoutInMillis
        )
        val exchangeRatesFetcher = RandomizingExchangeRatesRepository(NetworkExchangeRatesRepository(ratesAPI))
        val getExchangeUseCase = IntervalGetExchangeRatesUseCase(exchangeRatesFetcher, requestIntervalInMillis)
        val exchangeRatesInteractor = IntervalExchangeRatesInteractor(getExchangeUseCase)
        val stateToViewStateMapper = CachingStateToViewStateMapper()

        return CurrenciesViewModel(
            uiRefreshTimeInMillis,
            defaultBaseCurrency,
            defaultBaseValue,
            exchangeRatesInteractor,
            stateToViewStateMapper
        ) as T
    }
}