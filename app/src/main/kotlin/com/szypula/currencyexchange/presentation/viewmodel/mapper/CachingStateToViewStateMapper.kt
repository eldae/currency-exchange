package com.szypula.currencyexchange.presentation.viewmodel.mapper

import com.szypula.currencyexchange.domain.model.ExchangeRates
import com.szypula.currencyexchange.domain.model.ExchangeRatesState
import com.szypula.currencyexchange.domain.model.ExchangeRatesState.Error
import com.szypula.currencyexchange.domain.model.ExchangeRatesState.Loading
import com.szypula.currencyexchange.domain.model.ExchangeRatesState.Value
import com.szypula.currencyexchange.presentation.viewmodel.Currency
import com.szypula.currencyexchange.presentation.viewmodel.ViewState

class CachingStateToViewStateMapper : StateToViewStateMapper {

    private var currentViewState: ViewState = ViewState.EMPTY

    override fun map(state: ExchangeRatesState, value: String) = when (state) {
        is Loading -> currentViewState.copy(
            isLoading = true,
            isError = false
        )
        is Value -> currentViewState.copy(
            isLoading = false,
            currencies = state.exchangeRates.toCurrencyViewStates(value),
            isError = false
        )
        is Error -> currentViewState.copy(
            isLoading = false,
            isError = true
        )
    }.also(::cache)

    private fun ExchangeRates.toCurrencyViewStates(value: String): List<Currency> =
        if (rates.isNotEmpty()) {
            listOfCurrencies(value)
        } else {
            emptyList()
        }

    private fun ExchangeRates.listOfCurrencies(value: String): List<Currency> {
        val currencies = mutableListOf(Currency(
            base,
            value
        ))

        rates.forEach {
            currencies.add(Currency(
                it.name,
                it.value.formatToValue()
            ))
        }

        return currencies
    }

    private fun Double.formatToValue(): String = String.format("%.2f", this)


    private fun cache(viewState: ViewState) {
        currentViewState = viewState
    }
}