package com.szypula.currencyexchange.presentation.viewmodel

sealed class UIEvent {

    object CurrenciesShown: UIEvent()

    object CurrenciesHidden: UIEvent()

    data class CurrencySelected(
        val currency: String,
        val value: String
    ) : UIEvent()

    data class ValueChanged(val value: String): UIEvent()
}