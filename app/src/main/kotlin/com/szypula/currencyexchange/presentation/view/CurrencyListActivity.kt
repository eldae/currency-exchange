package com.szypula.currencyexchange.presentation.view

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.SimpleItemAnimator
import com.szypula.currencyexchange.App
import com.szypula.currencyexchange.R
import com.szypula.currencyexchange.presentation.viewmodel.CurrenciesViewModel
import com.szypula.currencyexchange.presentation.viewmodel.CurrenciesViewModelFactory
import com.szypula.currencyexchange.presentation.viewmodel.UIEvent
import com.szypula.currencyexchange.presentation.viewmodel.UIEvent.CurrenciesHidden
import com.szypula.currencyexchange.presentation.viewmodel.UIEvent.CurrenciesShown
import com.szypula.currencyexchange.presentation.viewmodel.UIEvent.CurrencySelected
import com.szypula.currencyexchange.presentation.viewmodel.UIEvent.ValueChanged
import kotlin.LazyThreadSafetyMode.NONE
import kotlinx.android.synthetic.main.activity_currency_list.currency_list as currencyList

class CurrencyListActivity : AppCompatActivity() {

    private val uiEvents = MutableLiveData<UIEvent>()

    private val currencyListAdapter by lazy(NONE) {
        CurrencyListAdapter(
            { currency, value -> uiEvents.value = CurrencySelected(currency, value) },
            { value -> uiEvents.value = ValueChanged(value) }
        )
    }

    private val viewModel by lazy(NONE) {
        ViewModelProviders
            .of(this, CurrenciesViewModelFactory((application as App).config))
            .get(CurrenciesViewModel::class.java)
    }

    private val uiEventObserver: Observer<UIEvent> by lazy(NONE) { viewModel }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_currency_list)
        initializeCurrencyList()

        uiEvents.observeForever(uiEventObserver)
    }

    private fun initializeCurrencyList() = with(currencyList) {
        setHasFixedSize(true)
        layoutManager = LinearLayoutManager(this@CurrencyListActivity)
        adapter = currencyListAdapter
        (itemAnimator as SimpleItemAnimator).supportsChangeAnimations = false
        addOnScrollListener(object : RecyclerView.OnScrollListener() {

            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                when (newState) {
                    RecyclerView.SCROLL_STATE_DRAGGING -> stopObservingViewState()
                    RecyclerView.SCROLL_STATE_IDLE -> observeViewState()
                }
            }
        })
    }

    override fun onStart() {
        super.onStart()

        observeViewState()
        uiEvents.value = CurrenciesShown
    }

    override fun onStop() {
        uiEvents.value = CurrenciesHidden

        super.onStop()
    }

    private fun observeViewState() {
        viewModel.viewState.observe(
            this,
            Observer {
                currencyList.post { currencyListAdapter.currencies = it.currencies }
            })
    }

    private fun stopObservingViewState() {
        viewModel.viewState.removeObservers(this@CurrencyListActivity)
    }
}