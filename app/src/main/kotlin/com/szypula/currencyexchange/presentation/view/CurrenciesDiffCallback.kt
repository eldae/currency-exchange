package com.szypula.currencyexchange.presentation.view

import androidx.recyclerview.widget.DiffUtil
import com.szypula.currencyexchange.presentation.viewmodel.Currency

class CurrenciesDiffCallback(
    private val old: List<Currency>,
    private val new: List<Currency>
) : DiffUtil.Callback() {

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean =
        old[oldItemPosition].name == new[newItemPosition].name

    override fun getOldListSize(): Int = old.size

    override fun getNewListSize(): Int = new.size

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean =
        old[oldItemPosition] == new[newItemPosition]
}