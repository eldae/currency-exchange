package com.szypula.currencyexchange.presentation.viewmodel

class ViewStateBuilder {

    private var currencies = emptyList<Currency>()
    private var isLoading = false
    private var isError = false

    fun withCurrencies(currencies: List<Currency>) = apply {
        this.currencies = currencies
    }

    fun withIsLoading(isLoading: Boolean) = apply {
        this.isLoading = isLoading
    }

    fun withIsError(isError: Boolean) = apply {
        this.isError = isError
    }

    fun build() = ViewState(currencies, isLoading, isError)
}

class CurrencyBuilder {

    private var name: String = ""
    private var value: String = ""

    fun withName(name: String) = apply {
        this.name = name
    }

    fun withValue(value: String) = apply {
        this.value = value
    }

    fun build() = Currency(name, value)
}

fun viewState() = ViewStateBuilder().build()