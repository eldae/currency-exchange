package com.szypula.currencyexchange.presentation.viewmodel.mapper

import com.szypula.currencyexchange.domain.ExchangeRatesBuilder
import com.szypula.currencyexchange.domain.model.ExchangeRatesState.Error
import com.szypula.currencyexchange.domain.model.ExchangeRatesState.Loading
import com.szypula.currencyexchange.domain.model.ExchangeRatesState.Value
import com.szypula.currencyexchange.domain.RateBuilder
import com.szypula.currencyexchange.presentation.viewmodel.CurrencyBuilder
import com.szypula.currencyexchange.presentation.viewmodel.ViewStateBuilder
import org.junit.Assert.assertEquals
import org.junit.Test

class CachingStateToViewStateMapperTest {

    private val tested = CachingStateToViewStateMapper()
    private val value = "1.5"

    @Test
    fun `maps to currencies`() {
        val actual = tested.map(valueWithExchangeRates(), value)

        val expected = ViewStateBuilder().withCurrencies(currencies()).build()
        assertEquals(expected, actual)
    }

    @Test
    fun `maps to loading`() {
        val actual = tested.map(Loading, value)

        val expected = ViewStateBuilder().withIsLoading(true).build()
        assertEquals(expected, actual)
    }

    @Test
    fun `maps to error`() {
        val actual = tested.map(Error(RuntimeException()), value)

        val expected = ViewStateBuilder().withIsError(true).build()
        assertEquals(expected, actual)
    }

    @Test
    fun `maps to loading with cached currencies`() {
        tested.map(valueWithExchangeRates(), value)

        val actual = tested.map(Loading, value)

        val expected = ViewStateBuilder()
            .withCurrencies(currencies())
            .withIsLoading(true)
            .build()
        assertEquals(expected, actual)
    }

    @Test
    fun `maps to error with cached currencies`() {
        tested.map(valueWithExchangeRates(), value)

        val actual = tested.map(Error(RuntimeException()), value)

        val expected = ViewStateBuilder()
            .withCurrencies(currencies())
            .withIsError(true)
            .build()
        assertEquals(expected, actual)
    }

    @Test
    fun `removes loading for new error state`() {
        tested.map(Loading, value)

        val actual = tested.map(Error(RuntimeException()), value)

        val expected = ViewStateBuilder()
            .withIsError(true)
            .withIsLoading(false)
            .build()
        assertEquals(expected, actual)
    }

    @Test
    fun `removes error for new loading state`() {
        tested.map(Error(RuntimeException()), value)

        val actual = tested.map(Loading, value)

        val expected = ViewStateBuilder()
            .withIsError(false)
            .withIsLoading(true)
            .build()
        assertEquals(expected, actual)
    }

    @Test
    fun `removes loading for new value state`() {
        tested.map(Loading, value)

        val actual = tested.map(valueWithExchangeRates(), value)

        val expected = ViewStateBuilder()
            .withIsLoading(false)
            .withCurrencies(currencies())
            .build()
        assertEquals(expected, actual)
    }

    @Test
    fun `removes error for new value state`() {
        tested.map(Error(RuntimeException()), value)

        val actual = tested.map(valueWithExchangeRates(), value)

        val expected = ViewStateBuilder()
            .withIsError(false)
            .withCurrencies(currencies())
            .build()
        assertEquals(expected, actual)
    }

    private fun valueWithExchangeRates() =
        Value(
            ExchangeRatesBuilder()
                .withBase("EUR")
                .withRates(
                    RateBuilder()
                        .withName("USD")
                        .withValue(2.1)
                        .build(),
                    RateBuilder()
                        .withName("PLN")
                        .withValue(4.8)
                        .build()
                )
                .build()
        )

    private fun currencies() = listOf(
        CurrencyBuilder()
            .withName("EUR")
            .withValue("1.5")
            .build(),
        CurrencyBuilder()
            .withName("USD")
            .withValue("2.10")
            .build(),
        CurrencyBuilder()
            .withName("PLN")
            .withValue("4.80")
            .build()
    )
}