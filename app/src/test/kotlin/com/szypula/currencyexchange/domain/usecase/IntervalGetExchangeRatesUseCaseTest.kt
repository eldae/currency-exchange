package com.szypula.currencyexchange.domain.usecase

import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import com.szypula.currencyexchange.domain.model.ExchangeRatesState.Loading
import com.szypula.currencyexchange.domain.model.ExchangeRatesState.Value
import com.szypula.currencyexchange.domain.exchangeRates
import com.szypula.currencyexchange.domain.repository.ExchangeRatesRepository
import io.reactivex.Single
import io.reactivex.schedulers.TestScheduler
import org.junit.Test
import java.util.concurrent.TimeUnit.MILLISECONDS

class IntervalGetExchangeRatesUseCaseTest {

    private val exchangeRatesRepositoryMock: ExchangeRatesRepository = mock()
    private val testScheduler = TestScheduler()
    private val intervalInMilliseconds = 1000L
    private val rates = exchangeRates()
    private val baseCurrency = "ANY"

    private val tested = IntervalGetExchangeRatesUseCase(
        exchangeRatesRepositoryMock,
        intervalInMilliseconds,
        testScheduler,
        testScheduler
    )

    @Test
    fun `emits loading at start`() {
        tested.observeState(baseCurrency)
            .test()
            .assertValue(Loading)
    }

    @Test
    fun `emits Loading and a Value for no time elapsed`() {
        whenever(exchangeRatesRepositoryMock.fetchExchangeRates(baseCurrency))
            .thenReturn(Single.just(rates))

        val observer = tested.observeState(baseCurrency).test()

        testScheduler.advanceTimeBy(0, MILLISECONDS)
        observer.assertValues(Loading, Value(rates))
    }

    @Test
    fun `emits Loading and Values once per every interval time elapsed plus once at start`() {
        whenever(exchangeRatesRepositoryMock.fetchExchangeRates(baseCurrency))
            .thenReturn(Single.just(rates))

        val observer = tested.observeState(baseCurrency).test()

        testScheduler.advanceTimeBy(intervalInMilliseconds * 2, MILLISECONDS)
        observer.assertValues(Loading, Value(rates), Value(rates), Value(rates))
    }

    @Test
    fun `does not complete after interval`() {
        whenever(exchangeRatesRepositoryMock.fetchExchangeRates(baseCurrency))
            .thenReturn(Single.just(rates))

        val observer = tested.observeState(baseCurrency).test()

        testScheduler.advanceTimeBy(intervalInMilliseconds, MILLISECONDS)
        observer.assertNotComplete()
    }

    @Test
    fun `retries on error after interval`() {
        whenever(exchangeRatesRepositoryMock.fetchExchangeRates(baseCurrency))
            .thenThrow(RuntimeException())
            .thenReturn(Single.just(rates))

        val observer = tested.observeState(baseCurrency).test()

        testScheduler.advanceTimeBy(intervalInMilliseconds, MILLISECONDS)
        observer
            .assertValues(Loading, Value(rates))
            .assertNoErrors()
            .assertNotComplete()
    }
}