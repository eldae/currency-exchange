package com.szypula.currencyexchange.domain

import com.szypula.currencyexchange.domain.model.ExchangeRates
import com.szypula.currencyexchange.domain.model.Rate

class ExchangeRatesBuilder {

    private var base: String = ""
    private var rates: List<Rate> = emptyList()

    fun withBase(base: String) = apply {
        this.base = base
    }

    fun withRates(vararg rates: Rate) = apply {
        this.rates = rates.asList()
    }

    fun build(): ExchangeRates = ExchangeRates(base, rates)
}

class RateBuilder {

    private var name: String = ""
    private var value: Double = 0.0

    fun withName(name: String) = apply {
        this.name = name
    }

    fun withValue(value: Double) = apply {
        this.value = value
    }

    fun build() = Rate(name, value)
}

fun exchangeRates() = ExchangeRatesBuilder().build()